# MDB Web Starter - Minimal

[![npm version](https://badge.fury.io/js/mdb-web-starter-minimal.svg)](https://badge.fury.io/js/mdb-web-starter-minimal)

This is a simple set of starter files for beginning a general-purpose, minimal configuration web project, which utilizes Gulp, Autoprefixer, Sass (SCSS), and npm.

### Features
* Responsive and scalable components
* CSS-only custom form controls
* Extendable breakpoint system based on the [Sass MQ mixin](http://sass-mq.github.io/sass-mq)
* Consistent vertical rhythm and modular scale


## Table of Contents
* [Installation](#installation)
* [Usage](#usage)
* [Breakpoints](#breakpoints)
* [Forms](#forms)
* [Browser Support](#browser-support)
* [Inspired By](#inspired-by)
* [License](#license)


## Installation

**Install from npm**

```sh
npm install mdb-web-starter-minimal
```

**Clone the Repo**

```sh
git clone -b master https://mattdanielbrown@bitbucket.org/mattdanielbrownteam/mdb-web-starter-minimal.git
```


## Usage

### Sass

Default variables can be changed before importing Baseguide.
Take a look at the [_variables.scss](./scss/partials/_variables.scss) file to get an overview of all variables.

```scss
// 1. Customize default variables in scss/partials/_variables.scss.
$button-bg: #bada55;

// 2. Add your own styles anywhere you like.
.my-custom-style {
	background: #fff;
	display: inline-block;
	// ... etc ...
}

// 3. If you want to add your own partials, just make sure to import it somewhere.
//	Example:
//		scss/main.scss
		@import "my-custom-partial";
```

### Gulp

The included gulpfile takes care of compiling, optimizing and minifying your assets. Running the following command will install all dependencies and start a local server using [Browsersync](https://www.browsersync.io/).

```sh
npm install && gulp
```

## Breakpoints
Breakpoints can easily be configured using the ```$mq-breakpoints``` map. Note that the breakpoints have to be sorted from small to large.

The default configuration looks like this:

```scss
$mq-breakpoints: (
  xs: 480px,
  sm: 768px,
  md: 992px,
  lg: 1200px
);
```

Baseguide generates all the necessary grid and responsive visibility classes based on these breakpoints.

### Media Queries
Media Queries are handled by [Sass MQ](https://github.com/sass-mq/sass-mq).

```scss
// include the media query mixin and pass the breakpoint key
@include mq(md) {
  
}
```

The snippet above compiles to the following CSS:

```css
@media (min-width: 62em) {

}
```

Check out the [Sass MQ documentation](http://sass-mq.github.io/sass-mq/#mixin-mq) for more details and advanced usage of media queries.

#### Legacy Support
To support browsers without native media query support you could use [respond.js](https://github.com/scottjehl/Respond).

A static solution without Javascript is possible by setting ```$mq-responsive``` to ```false```. The code below generates an additional stylesheet where only styles in large (lg) media queries are included.

```scss
// oldie.scss
$mq-responsive: false;
$mq-static-breakpoint: lg;

@import 'main';
```

Include the generated CSS file after the rest of your styles to serve a fixed width layout to legacy browsers.
```html
<!--[if lt IE 9]><link rel="stylesheet" href="css/oldie.css"><![endif]-->
```

### Breakpoint Loop
The ```loop-breakpoints``` mixin iterates through all breakpoints. It sets three global variables and outputs the ```@content``` for each breakpoint.
```scss
@include loop-breakpoints($mq: true, $inclusive: false, $breakpoint-keys: $mq-breakpoints-list) {
  @debug $breakpoint;
  @debug $is-first-breakpoint;
  @debug $is-last-breakpoint;
}
```

It’s a powerful tool that for example allows the generation of additional responsive helper classes.
```scss
@include loop-breakpoints {
  .text-#{$breakpoint}-left {
    text-align: left;
  }

  .text-#{$breakpoint}-center {
    text-align: center;
  }

  .text-#{$breakpoint}-right {
    text-align: right;
  }
}
```


## Forms

### Standard Form Controls
All form controls listed in ```$input-selector``` get styled by default. The variable can be changed to a custom selector like ```.form-control```. This will allow you to selectively style form controls based on that selector.

Remember to reset the height of textareas if you choose a custom selector:

```scss
textarea.form-control {
  height: auto;
}
```

### Custom Form Controls
The custom forms component was designed with progressive enhancement in mind.
While the controls are functional in all browsers the following ones get the fully enhanced experience:

* Android 2.3+
* Chrome
* Firefox 35+
* IE 10+
* Mobile Safari 4+
* Safari 5.1+
* Opera 15+

You can set the variable ```$use-custom-forms``` to ```false``` to disable custom form styles in all browsers.

### Caveats
In iOS versions prior to 5.1.1 the code below is required to make custom radio buttons and checkboxes work.

```js
if (document.addEventListener) {
  document.addEventListener('click', function() {}, false);
}
```


## Browser Support
* Latest stable: Chrome, Firefox, Opera
* IE 8+
* Safari 6+
* Mobile Safari 6+
* Android Browser 2.3+

Baseguide uses [Autoprefixer](https://github.com/postcss/autoprefixer) to handle CSS vendor prefixes.


## Inspired By…
* [Baseguide](http://basegui.de)
* [Bourbon](http://bourbon.io)
* [Bitters](http://bitters.bourbon.io)
* [HTML5 Boilerplate](https://html5boilerplate.com)


## License
The code is released under the [MIT license](https://github.com/slavanga/baseguide/blob/master/LICENSE).
